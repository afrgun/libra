angular.module('starter')

.directive('ionSearch', function() {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            getData: '&source',
            model: '=?',
            search: '=?filter'
        },

        link: function(scope, element, attrs) {
            attrs.minLength = attrs.minLength || 0;
            scope.placeholder = attrs.placeholder || '';
            scope.search = {value: ''};

            if (attrs.class)
                element.addClass(attrs.class);

            if (attrs.source) {
                scope.$watch('search.value', function (newValue, oldValue) {
                    if (newValue.length > attrs.minLength) {
                        scope.getData({str: newValue}).then(function (results) {
                            scope.model = results;
                        });
                    } else {
                        scope.model = [];
                    }
                });
            }

            scope.clearSearch = function() {
                scope.search.value = '';
            };
        },
        template: '<div class="item-input-wrapper">' +
            '<input type="search" placeholder="{{placeholder}}" ng-model="search.value">' +
            '</div>'
    };
})

.directive('search', function(){
    return{
        restrict: 'C',
        compile: function (element, attr){
            var contents = element.html();
            element.html('<div class="search-input" style="margin:0 !important; padding:0 !important;" >' + contents + '</div>');

            var shopclose = element[0].querySelector('.ion-close');

            return function postLink(scope, element, attrs) {
                angular.element(shopclose).css({
                    'visibility': 'hidden',
                });

                element.css({
                    'width': 0,
                    'transitionDuration': '500ms',
                    'transitionTimingFunction': 'ease-in-out',
                });
            };
        }
    };
})

.directive('sliderToggle', function(){
    return{
        restrict: 'A',
        link: function($scope, element, attrs){
            attrs.expanded = false;

            element.off("click").bind('click', function(e) {
                var target = document.querySelector(attrs.sliderToggle);

                //var shopclose = document.querySelector('.ion-close');
                //var shopsearch = document.querySelector('.ion-search');

                if(!attrs.expanded ){
                    target.style.width = '100%';
                    angular.element(element).removeClass(attrs.onIcon);
                    angular.element(element).addClass(attrs.offIcon);
                    //shopclose.style.visibility = 'visible';
                    //shopsearch.style.visibility = 'hidden';
                } else {
                    target.style.width = '0px';
                    angular.element(element).removeClass(attrs.offIcon);
                    angular.element(element).addClass(attrs.onIcon);
                    //shopclose.style.visibility = 'hidden';
                    //shopsearch.style.visibility = 'visible';
                }

                attrs.expanded = !attrs.expanded;
            });
        }
    };
})

/* =========== FOCUS INPUT/TEXTAREA =========== */
.directive('foc', function($timeout){
    return {
        restrict: 'C',
        //scope: { trigger: '=focusMe' },
        link: function(scope, element, attrs) {

            var focuser = document.querySelector('.foc');
            scope.comment = function() {
                $timeout(function() {
                    focuser.focus(); 
                },50)
            };
            console.log(focuser);

            /*scope.$watch( function(){
                $timeout(function(){
                    focuser.focus()
                }, 50);
            })*/

        }
    }
}) 
/* ================ CLOSE ===================== */

/* ============== LIKE HEART ================== */
.directive('toggleClass', function(){
    return{
        restrict: 'A',
        link: function(scope, element, attrs){
            element.bind('click', function(){
                element.toggleClass(attrs.toggleClass);
            });
        }
    }
})
/* ============ CLOSE LIKE HEART ============== */



