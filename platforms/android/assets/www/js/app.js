// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic', 'starter.controllers'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
  $ionicConfigProvider.tabs.position('bottom');
  $stateProvider


  /* === tab start === */
  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/master-tab.html'
  })

  .state('app.timeline', {
    cache: false,
    url: '/timeline',
    views: {
      'tab-timeline': {
        templateUrl: 'templates/timeline/timeline.html',
        controller: 'TimelineCtrl',
      }
    }
  })

  .state('app.mylibra', {
    url: '/mylibra',
    views: {
        'tab-mylibra': {
          templateUrl: 'templates/mylibra/mylibra.html',
          controller: 'MylibraCtrl',
        }
    }
  })

  .state('app.mylibra-detail', {
      url: '/detail-mylibra',
      views: {
        'tab-mylibra':{
          templateUrl: 'templates/mylibra/detail-mylibra.html',
          controller: 'MylibraCtrl',
        }
      }
  })

  .state('app.addlibra',{
    url: '/addlibra',
    views: {
      'tab-addlibra':{
        templateUrl: 'templates/addlibra/add-libra.html',
        controller: '',
      }
    }
  })

  .state('addbook',{
    url: '/addbook',
    views: {
      '':{
          templateUrl: 'templates/addlibra/add-book.html',
          controller: '',
      }
    }
  })

  $urlRouterProvider.otherwise('/app/timeline');

});