angular.module('starter.controllers', [])

.controller('TimelineCtrl', function($rootScope, $scope, $ionicPopup, $timeout){
	
	
	$scope.addLibra = function() {
	  $scope.data = {};


	  // An elaborate, custom popup
	 var tesadd = $ionicPopup.show({
	    template: '<input type="password" ng-model="data.wifi">',
	    title: 'Enter Wi-Fi Password',
	    subTitle: 'Please use normal things',
	    scope: $scope,
	    buttons: [
	      { text: 'Cancel' },
	      {
	        text: '<b>Save</b>',
	        type: 'button-positive',
	        onTap: function(e) {
	          if (!$scope.data.wifi) {
	            //don't allow the user to close unless he enters wifi password
	            e.preventDefault();
	          } else {
	            return $scope.data.wifi;
	          }
	        }
	      }
	    ]
	  })
	}

})

.controller('MylibraCtrl', function($rootScope, $scope, $location, $timeout){

	
	$scope.detmylibra = function(path){

		$location.path(path);
		/*var element = document.querySelector('textarea');
		$scope.comment = function() {
            $timeout(function() {
                element.focus(); 
            },50)

        };
        console.log(element);
        console.log($location.path());*/
    }; 

})